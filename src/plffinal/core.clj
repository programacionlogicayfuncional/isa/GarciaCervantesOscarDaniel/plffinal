(ns plffinal.core)

;Primera funcion del examen
(defn regresa-al-punto-de-origen?
  [s]
  (let [a (fn [] (get (frequencies s) \^ 0))
        b (fn [] (get (frequencies s) \v 0))
        i (fn [] (get (frequencies s) \< 0))
        d (fn [] (get (frequencies s) \> 0))
        r (fn [] (and (== (a) (b)) (== (i) (d))))]
    (r)))

;Segundo problema del examen
(defn regresan-al-punto-de-origen?
  [& args]
  (let [r  (fn [] (every? true? (map regresa-al-punto-de-origen? args)))]
    (r)))

;Tercer problema

(defn tabla-conv
  []
  (hash-map \> \<
            \< \>
            \v \^
            \^ \v))

(defn cambiar
  [xs]
  (let [tabla (tabla-conv)
        convertir (fn [c] (let [v (tabla c)] (if (char? v) v c)))]
    (convertir xs)))

(defn regreso-al-punto-de-origen
  [s]
  (let [r  (fn [] (map cambiar (reverse s)))]
    (if (true? (regresa-al-punto-de-origen? s)) (list) (r))))

;Cuarto problema
(defn mismo-punto-final?
  [z w]
  (let [a (fn [s] (get (frequencies s) \^ 0))
        b (fn [s] (get (frequencies s) \v 0))
        i (fn [s] (get (frequencies s) \< 0))
        d (fn [s] (get (frequencies s) \> 0))
        x (fn [s] (- (d s) (i s)))
        y (fn [s] (- (a s) (b s)))
        r (fn [] (and (== (x z) (x w)) (== (y z) (y w))))]
    (r)))

;Quinto problema
(defn tabla-pos
  []
  (hash-map \< [-1 0]
            \v [0 -1]
            \> [1 0]
            \^ [0 1]))

(defn xy
  [x]
  (letfn [(p [s] (replace (tabla-pos) s))
          (f [sec acc]
            (if (empty? sec)
              [[0 0]]
              (g (conj acc [0 0] (first (p sec))) (rest (p sec)))))

          (g [aux tp]
            (if (empty? tp)
              aux
              (g (conj aux [(+ (first (last aux)) (first (first tp))) (+ (second (last aux)) (second (first tp)))]) (rest tp))))]
    (f x [])))

(defn coincidencias
  [sa sb]
  (let [f (fn [] (concat (xy sa) (xy sb)))
        g (fn [] (vals (frequencies (f))))
        h (fn [] (count (filterv (fn [x]
                                   (> x 1)) (g))))]
    (h)))
