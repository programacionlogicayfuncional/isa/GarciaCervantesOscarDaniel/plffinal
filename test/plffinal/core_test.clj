(ns plffinal.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [plffinal.core :refer :all]))

;Primera funcion test
(deftest punto-origen?-test-v
  (testing "regresa a punto de origen? casos verdaderos"
    (is (= true (regresa-al-punto-de-origen? "")))
    (is (= true (regresa-al-punto-de-origen? [])))
    (is (= true (regresa-al-punto-de-origen? (list))))
    (is (= true (regresa-al-punto-de-origen? "><")))
    (is (= true (regresa-al-punto-de-origen? (list \> \<))))
    (is (= true (regresa-al-punto-de-origen? "v^")))
    (is (= true (regresa-al-punto-de-origen? [\v \^])))
    (is (= true (regresa-al-punto-de-origen? "^>v<")))
    (is (= true (regresa-al-punto-de-origen? (list \^ \> \v \<))))
    (is (= true (regresa-al-punto-de-origen? "<<vv>>^^")))
    (is (= true (regresa-al-punto-de-origen? [\< \< \v \v \> \> \^ \^])))))

(deftest punto-origen?-test-f
  (testing "regresa a punto de origen? casos falsos"
    (is (= false (regresa-al-punto-de-origen? ">")))
    (is (= false (regresa-al-punto-de-origen? (list \>))))
    (is (= false (regresa-al-punto-de-origen? "<^")))
    (is (= false (regresa-al-punto-de-origen? [\< \^])))
    (is (= false (regresa-al-punto-de-origen? ">>><<")))
    (is (= false (regresa-al-punto-de-origen? (list \> \> \> \< \<))))
    (is (= false (regresa-al-punto-de-origen? [\v \v \^ \^ \^])))
    (is (= false (regresa-al-punto-de-origen? "^^>>><<>>>")))
    (is (= false (regresa-al-punto-de-origen? (list \^ \> \v \> \> \< \v \v))))
    (is (= false (regresa-al-punto-de-origen? "<<vv>>^^>>>>><<<vvvvv")))
    (is (= false (regresa-al-punto-de-origen? [\< \< \v \v \v \v])))))

;Segunda funcion test
(deftest puntos-origen?-test-v
  (testing "Regresan al punto de origen? casos verdaderos"
    (is (= true (regresan-al-punto-de-origen?)))
    (is (= true (regresan-al-punto-de-origen? [])))
    (is (= true (regresan-al-punto-de-origen? "")))
    (is (= true (regresan-al-punto-de-origen? [] "" (list))))
    (is (= true (regresan-al-punto-de-origen? "" "" "" "" [] [] [] (list) "")))
    (is (= true (regresan-al-punto-de-origen? ">><<" [\< \< \> \>] (list \^ \^ \v \v))))))

(deftest puntos-origen?-test-f
  (testing "Regresan al punto de origen? casos falsos"
    (is (= false (regresan-al-punto-de-origen? (list \< \>) "^^" [\> \<])))
    (is (= false (regresan-al-punto-de-origen? ">>>" "^vv^" "<<>>")))
    (is (= false (regresan-al-punto-de-origen? [\< \< \> \> \> \> \> \> \> \>])))
    (is (= false (regresan-al-punto-de-origen? [] "><>" (list \> \<))))))

;Tercer problema test

(deftest punto-origen-test-v
  (testing "regreso al punto de origen Casos que regesan vacio"
    (is (= () (regreso-al-punto-de-origen "")))
    (is (= () (regreso-al-punto-de-origen (list \^ \^ \^ \> \< \v \v \v))))))

(deftest punto-origen-test-l
  (testing "regreso al punto de origen Casos que regresan al punto"
    (is (= (list \< \< \<) (regreso-al-punto-de-origen ">>>")))
    (is (= (list \< \< \^ \^ \^ \>) (regreso-al-punto-de-origen [\< \v \v \v \> \>])))))

;Cuarto problema test

(deftest mismo-punto-test-v
  (testing "Mismo punto final Casos verdaderos"
    (is (= true (mismo-punto-final? "" [])))
    (is (= true (mismo-punto-final? "^^^" "<^^^>")))
    (is (= true (mismo-punto-final? [\< \< \< \>] (list \< \<))))
    (is (= true (mismo-punto-final? (list \< \v \>) (list \> \v \<))))))

(deftest mismo-punto-test-f
  (testing "Mismo punto final Casos falsos"
    (is (= false (mismo-punto-final? "" "<")))
    (is (= false (mismo-punto-final? [\> \>] "<>")))
    (is (= false (mismo-punto-final? [\> \> \>] [\> \> \> \>])))
    (is (= false (mismo-punto-final? (list) (list \^))))))

;Quinto problema test

(deftest punto-origen-test-u
  (testing "coincidencias que solo tienen el punto de origen"
    (is (= 1 (coincidencias "" [])))
    (is (= 1 (coincidencias (list \< \<) [\> \>])))))

(deftest punto-origen-test-m
  (testing "coincidencias que tienen m[as puntos aparte del origen"
    (is (= 2 (coincidencias [\^ \> \> \> \^] ">^^<")))
    (is (= 4 (coincidencias "<<vv>>^>>" "vv<^")))
    (is (= 6 (coincidencias ">>>>>" [\> \> \> \> \>])))
    (is (= 6 (coincidencias [\> \> \> \> \>] (list \> \> \> \> \> \> \^ \^ \^ \^))))))